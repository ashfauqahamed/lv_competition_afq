import torch
print(torch.cuda.is_available())


import pandas as pd
import os
from glob import glob

    
movie_detail_loc =r"C:\Users\Ahamed\Desktop\Webscraping for Techflix\movie_details"
all_files = glob(movie_detail_loc + "/*.csv")

li = []

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)
frame.to_csv("raw_data.csv",index = None)

import pandas as pd
import os
from glob import glob
import datetime
from pandas.tseries.holiday import USFederalHolidayCalendar
from sklearn.metrics import pairwise_distances
from sklearn.metrics.pairwise import cosine_similarity
cal = USFederalHolidayCalendar()
holidays = cal.holidays(start='2000-01-01', end='2023-12-31').to_pydatetime()

from pandas.core.dtypes.missing import isna
df = pd.read_csv('raw_data_from_2000.csv')
df1 = pd.read_csv('raw_data.csv')
df = pd.concat([df,df1],axis = 0)
df.to_csv('raw_data_from_2010.csv',index = None)
df = pd.read_csv('raw_data_from_2010.csv')

df = df.drop(['Unnamed: 0'],axis = 1)
print(df.shape)
df = df.drop_duplicates().reset_index(drop = True)
print(df.shape)


df = df[df['Opening'].isna() == False]
df = df[df['Genres'].isna() == False]
df = df[df["is-rerelease"] == 0]
df = df.drop_duplicates()

##
opening_hard_limit = 1
print(df['MPAA'].value_counts())

df = df.fillna(0)
df['Opening'] = [int(str(each).replace('$','').replace(',','')) for each in df['Opening']]
print(df.shape)
# df['Opening'].plot.hist(bins = 200)
df = df[df['Opening']>opening_hard_limit]
print(df.shape)
# df['Opening'].plot.hist(bins = 200)
df.MPAA.value_counts()


df = df.reset_index(drop = True)
print(df['MPAA'].value_counts())

## Release date cleaning

df['Release Date'] = [each.split("-")[0] if each != 0 else df['Release Date (Wide)'][i] for i,each in enumerate(df['Release Date'])]
df['Release Date'] = [each.split("(")[0] if each != 0 else df['Release Date (Wide)'][i] for i,each in enumerate(df['Release Date'])]
print(df.shape)
df = df[df['Release Date']!=0]
df = df.reset_index(drop = True)
print(df.shape)



holidays = holidays.tolist()
add_one_holiday = [each + datetime.timedelta(1) for each in holidays]
sub_one_holiday = [each - datetime.timedelta(1) for each in holidays]
add_two_holiday = [each + datetime.timedelta(2) for each in holidays]
sub_two_holiday = [each - datetime.timedelta(2) for each in holidays]
[holidays.append(each) for each in add_one_holiday]
[holidays.append(each) for each in sub_one_holiday]
[holidays.append(each) for each in add_two_holiday]
[holidays.append(each) for each in sub_two_holiday]

df['Release Date']  = [pd.to_datetime(each) for each in df['Release Date']]
df['is_weekend']  = [1 if pd.to_datetime(each).weekday() >= 5 else 0 for each in df['Release Date']]
df['is_holiday'] = [1 if each in holidays else 0 for each in df['Release Date']]
df['is_holiday'].value_counts()
df['Release Month'] = [(each.month) for each in df['Release Date']]
df = df[df['Release Date']>= pd.to_datetime('2010-01-01')]  ####################### Filtering the data for data from 2010
df = df.reset_index(drop = True)



## MPAA Rating
from sklearn.preprocessing import OneHotEncoder
mpaa_enc = OneHotEncoder()
# X = [['Male', 1], ['Female', 3], ['Female', 2]]
# print(X)
import numpy as np
mpaa = np.array(df['MPAA'].tolist())
print(mpaa.shape)
mpaa_enc.fit(mpaa.reshape(mpaa.shape[0],1))
df_mpaa= pd.DataFrame(mpaa_enc.transform(mpaa.reshape(mpaa.shape[0],1)).todense())
df_mpaa.columns = mpaa_enc.get_feature_names()
df_mpaa
# print(df_mpaa.shape)
# print(df.shape)
df = df.reset_index(drop = True)
df = pd.concat([df,df_mpaa],axis = 1)
# print(df.shape)
# print(df.shape)
# print(df.head(3))


## Genres splitting
all_generes = []
[all_generes.append(e) for each in df['Genres']  for e  in  str(each).split(" ")]

all_generes = list(set(all_generes))
all_generes
for gen in all_generes:
  df[gen] = [10-(each.split(" ").index(gen)) if gen in each.split(" ") else 0 for each in df['Genres']]
# df['Sci-Fi'].value_counts()

print('Genre Splitting done')

## Cast splitting
all_casts = []
[all_casts.append(e) for each in df['cast']  for e  in  str(each).split(";")]

all_casts = list(set(all_casts))
all_casts
for gen in all_casts:
  df["c_"+ gen] = [10-(each.split(";").index(gen)) if gen in each.split(";") else 0 for each in df['cast']]
all_casts

df['c_Sylvester Stallone'].value_counts()

print('cast Splitting done')

## Director splitting
all_director = []
[all_director.append(e) for each in df['Director']  for e  in  str(each).split(";")]

all_director = list(set(all_director))
all_director
for gen in all_director:
  df["dir_"+ gen] = [3-(str(each).split(";").index(gen)) if gen in str(each).split(";") else 0 for each in df['Director']]
all_director

df['dir_Christopher Nolan'].value_counts()

print('dir Splitting done')

## Writer splitting
all_writer = []
[all_writer.append(e) for each in df['Writer']  for e  in  str(each).split(";")]

all_writer = list(set(all_writer))
all_writer
for gen in all_writer:
  df["wri_"+ gen] = [3-(str(each).split(";").index(gen)) if gen in str(each).split(";") else 0 for each in df['Writer']]
all_writer

print('writer Splitting done')

# ## Composer
# all_composer = []
# [all_composer.append(e) for each in df['Composer']  for e  in  str(each).split(";")]

# all_composer = list(set(all_composer))
# all_composer
# for gen in all_composer:
#   df["com_"+ gen] = [8-(str(each).split(";").index(gen)) if gen in str(each).split(";") else 0 for each in df['Composer']]
# all_composer
# print('composer Splitting done')

# ## Composer
# all_cine = []
# [all_cine.append(e) for each in df['Cinematographer']  for e  in  str(each).split(";")]

# all_cine = list(set(all_cine))
# all_cine
# for gen in all_cine:
#   df["cine_"+ gen] = [8-(str(each).split(";").index(gen)) if gen in str(each).split(";") else 0 for each in df['Cinematographer']]
# all_cine
# print('cine Splitting done')

## Columns to Remove
rem_col = ['IMDbPro','Widest Release', 'Genres', 'MPAA','is-rerelease','year_release','cast',	'Director',	'Writer','Composer','Cinematographer','Editor','Running Time','In Release','Release Date (Wide)','is_weekend','is_holiday','Release Month','Release Date']
all_col = df.columns.tolist()
all_col = [each for each in all_col if each not in rem_col]
print(df.shape)
df = df[all_col]
print(df.shape)
df.to_csv('temp_features_from_2010.csv',index = None)


import pandas as pd
import os
from glob import glob
import datetime
from pandas.tseries.holiday import USFederalHolidayCalendar
from sklearn.metrics import pairwise_distances
from sklearn.metrics.pairwise import cosine_similarity
from sentence_transformers import SentenceTransformer
df = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/temp_features_from_2010.csv',index_col = None)

model_name = 'bert-large-nli-mean-tokens'
model_name = 'xlnet-large-cased'
model_name = 'all-mpnet-base-v2'
model = SentenceTransformer(model_name)
b = df.copy()
# b = b[['synopsis']]
print(b['synopsis'][1])
sent_vec = model.encode(b['synopsis'])
# print(b.columns)
b.shape
(sent_vec.shape)
b['sent_vec'] = [each.tolist() for each in sent_vec]

import pickle
with open('/content/drive/MyDrive/Colab Notebooks/sent_vec_from_2010.pik','wb') as F:
  pickle.dump(sent_vec,F)

with open('/content/drive/MyDrive/Colab Notebooks/mpaa_enc_from_2010.pik','wb') as F:
  pickle.dump(mpaa_enc,F)