****TechFlix Deeplearning based Movie Recommendation****

Recommendation Model Built for movies released after 2010 (Includes only the data from boxofficemojo.com) 

_**Steps for Deploying the model**_
-     1) Download the "Code Files/TechFlix_App_Deploy_v1.ipynb"
-     2) Possibly open the code file on Google Colab (Faster to run)
-     3) On executing the code the a ngrok link will be generated
-     4) Clicking on the ngrok link will take you to the frontend of the API
-     5) The user can post their requests for the API using the interface 




