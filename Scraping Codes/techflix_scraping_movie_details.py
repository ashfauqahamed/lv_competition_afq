
import pandas as pd
import selenium
from selenium import webdriver
import pandas as pd
import os

movie_link_loc = r"C:\Users\Ahamed\Desktop\Webscraping for Techflix\movie_links"
browser = webdriver.Chrome(executable_path=r"./chromedriver.exe")

err_list = []

for file in os.listdir(movie_link_loc):
    print("XXXXXXXXXX")
    print(file)
    filename = file
    file = (os.path.join(movie_link_loc,file))
    
    file_df = pd.read_csv(file)

    json_bucket = []    
    
    for main_link in pd.read_csv(file)['movie_links']:
        
        print(main_link)
        # main_link = "https://www.boxofficemojo.com/release/rl3412165377/?ref_=bo_rs_table_2"
        # main_link = "https://www.boxofficemojo.com/release/rl2337506049/?ref_=bo_rs_table_29"
        # weekend/?ref_=bo_rl_tab#tabs
        
        
        try:
        
            browser.get(main_link)
            
            main_card = browser.find_elements_by_css_selector(".a-fixed-left-grid-col.a-col-right")
            
            title = main_card[0].text.split('\n')[0]
            print(title)
            year_release = 'N/A' if len(main_card[0].text.split('\n')[1])> 50 else main_card[0].text.split('\n')[1]
            synopsis = main_card[0].text.split('\n')[1] if year_release == 'N/A'  else main_card[0].text.split('\n')[2]
            is_rerelease = 1 if 'Re-release' in main_card[0].text.split('\n')[1] else 0
            
            ans_json = { 'title':title, 'year_release':year_release, 'synopsis':synopsis, 'is-rerelease' :is_rerelease     }
            
            
            summary_card = browser.find_element_by_css_selector(".a-section.a-spacing-none.mojo-summary-values.mojo-hidden-from-mobile")
            
            sum_json = {each.text.split('\n')[0]:each.text.split('\n')[1] for each in summary_card.find_elements_by_css_selector(".a-section.a-spacing-none")}
            
            ans_json.update(sum_json)
            
            (ans_json)
            
            title_summary_button = browser.find_element_by_css_selector(".a-link-normal.mojo-title-link.refiner-display-highlight")
            title_summary_button.get_attribute('href')
            title_summary_button.click()
            cast_and_crew = browser.find_element_by_css_selector(".a-size-base.a-link-normal.mojo-navigation-tab")
            cast_link = cast_and_crew.get_attribute('href')
            cast_link = cast_link.split('?')[0] +"credits/?" + cast_link.split('?')[1]
            cast_link
            browser.get(cast_link)
            
            crew = []
            role = []
            crew_page = browser.find_element_by_id("principalCrew")
            for row in crew_page.find_elements_by_css_selector('tr')[1:]:
                crew_roles = row.find_elements_by_css_selector('td')
                crew.append(crew_roles[0].text)
                role.append(crew_roles[1].text)
                
            assert len(crew) == len(role)
            
            cast = []
            cast_page = browser.find_element_by_id("principalCast")
            for row in cast_page.find_elements_by_css_selector('tr')[1:]:
                cast_roles = row.find_elements_by_css_selector('td')
                cast.append(cast_roles[0].text)
            
            casts = ";".join(cast[:4]) 
            
            
            crews = {}
            for i,e in enumerate(role):
                if e.lower() in ('writer','composer','director','cinematographer','editor'):
                    if e in crews.keys():
                        crews[e] = crews[e]+";"+crew[i]
                    else:
                        crews[e] = crew[i]
            
            ans_json['cast']=casts
            ans_json.update(crews)
            ans_json
            
            json_bucket.append(ans_json)
        except:
            err_list.append((file,title))
            pass
        
    data_per_file = pd.DataFrame(json_bucket)
    data_per_file.to_csv(r"C:\Users\Ahamed\Desktop\Webscraping for Techflix\movie_details\{}.csv".format(filename))

