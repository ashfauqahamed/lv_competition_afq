import pandas as pd
import selenium
from selenium import webdriver
import pandas as pd

month_start_dates = []
for year in range(2000,2015):
    # print(year)
    for month in range(1,13):
        # print(month)
        
        txt = str(year)+"-"+str(month)+"-01"
        # print(txt)
        month_start_dates.append(txt)
        

browser = webdriver.Chrome(executable_path=r"./chromedriver.exe")


for month_start_date in month_start_dates:
    

    main_link = "https://www.boxofficemojo.com/calendar/{}/".format(month_start_date)
    print(main_link)
    browser.get(main_link)
    
    release_label = browser.find_elements_by_class_name("mojo-group-label")
    movie_bucket = browser.find_elements_by_css_selector(".a-section.a-spacing-none.mojo-schedule-release-details")
    movie_links = [each.find_elements_by_class_name("a-link-normal")[0].get_attribute('href') for each in movie_bucket]
    movie_names = [each.find_elements_by_class_name("a-link-normal")[0].text for each in movie_bucket]
    
    assert len(movie_bucket)== len(movie_links)
    assert len(movie_links)== len(movie_names)

    ans = pd.DataFrame()
    ans["movie_names"] = movie_names
    ans["movie_links"] = movie_links
    ans['release_month'] = month_start_date 
    
    
    ans.to_csv(r"C:\Users\Ahamed\Desktop\Webscraping for Techflix\movie_links\{}.csv".format(month_start_date))

#%%



